<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot Password</title>
</head>
<body>
    
</body>
<form action="{{ url('/forgot_password') }}" method="post">

{{ csrf_field() }}

    @if(session('error'))
       <div>{{ session('error') }}</div>
    @endif

    @if(session('success'))
       <div>{{ session('success') }}</div>
    @endif

    <input type="email" name="email" id="email" placeholder="Enter your E-mail" value="{{ old('email') }}">
    <button type="submit">Submit</button>

</form>
</html>