<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Reminder;
use App\User;
use Activation;
use Mail;

class ForgotPassword extends Controller
{
    public function forgot(){
        return view('security.forgot');
    }

    public function password(Request $request){
        $user = User::whereEmail($request->email)->first();

        if($user == null){
            return redirect()->back()->with(['error'=> 'Email not exists']);
        }

        $user = Sentinel::findById($user->id);
        $reminder = Reminder::exists($user) ? : Reminder::create($user);
        $this->sendEmail($user, $reminder['code']);

        return redirect()->back()->with(['success'=> 'Reset code sent to your email.']);
    }

    public function sendEmail($user, $code){  
        Mail::send(                        
            'email.forgot',
            ['user'=> $user,'code'=> $code],
            function($message) use ($user){
                $message->to($user->email);
                $message->subject("$user->name, reset you password."); 
            }
        );
    }

    public function reset($email, $code){  
        $user = User::whereEmail($email)->first();  
        
        if($user == null){
            echo 'Email not exists';
        }

        $user = Sentinel::findById($user->id);
        $reminder = Reminder::exists($user);   

        if($reminder){  
            if($code == $reminder->code){    
                return view('security.reset_password_form')->with(['user'=>$user,'code'=>$code]); 
            }else{   
                return redirect('/');
            }
        }else{
            echo 'time expired';
        }
    }

    public function resetPassword(Request $request, $email, $code){
        $this->validate($request,[
            'password'=> 'required|min:5|max:12|confirmed',
            'password_confirmation'=> 'required|min:5|max:12'
        ]);

        $user = User::whereEmail($email)->first();
        
        if($user == null){
            echo 'Email not exists';
        }
        
        $user = Sentinel::findById($user->id);
        $reminder = Reminder::exists($user);


        if($reminder){
            if($code == $reminder->code){
                Reminder::complete($user, $code, $request->password);
                return redirect('/login')->with('success', 
                     'Password reset. Please login with new password.');
            }else{
                return redirect('/');
            }
        }else{
            echo 'time expired';
        }
    }
}
