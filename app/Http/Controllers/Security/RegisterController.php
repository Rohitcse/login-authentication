<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Activation;
use App\Models\User;
use App\Models\Roles\RoleModel;
use Mail;

class RegisterController extends Controller
{
    public function register(){
        $data['roles'] = RoleModel::get();
        return view('security.register')->with('data', $data);
    }

    public function registerUser(Request $request){
        $data = $request->all();
        $roleID = $data ['role'];
        
             $user = Sentinel::register($request-> all());
             
             
             $role = Sentinel::findRoleById($roleID);
             $role->users()->attach($user);

             $activate = Activation::create($user);
             $this->sendActivationEmail($user, $activate->code);

             echo 'User Registered';
             return redirect('/');
    }

    public function sendActivationEmail($user, $code){
        Mail::send(
            'email.activation',
            ['user' => $user,'code' => $code],
            function($message) use ($user){
                $message->to($user->email);
                $message->subject("Hello $user->name", "Activate your account.");
            }
        );
    }

}
