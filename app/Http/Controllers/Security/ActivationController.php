<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Activation;
use App\User;

class ActivationController extends Controller
{
    public function activate($email , $code){
        $user = User::whereEmail($email)->first();
        $user = Sentinel::findById($user->id);
            
        if(Activation::complete($user , $code)){
            return redirect('/login');
        }
    }
}
