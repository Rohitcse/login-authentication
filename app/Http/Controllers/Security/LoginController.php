<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Validator;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class LoginController extends Controller
{
    public function login(){
        if(Sentinel::check()){
            return redirect(url('/'));
        }
        return view('security.login');
    }

    public function postLogin(Request $request){
         //Sentinel::disableCheckpoints();
         $errorMsgs = [
            'email.required' => 'Please Provide email id',
            'email.email' => 'The email must be a valid email',
            'password.required' => 'Password is required'

         ];
         $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'

         ], $errorMsgs);

         if($validator->fails()){
             $returnData = array(
                 'status' => 'error',
                 'message' => 'Please review fields',
                 'errors' => $validator->errors()->all()
             );
             //return response()->json($returnData, 500);
             return redirect()->back()->with(['error'=>$validator->errors()->all()]);
         }
         if($request->remember == 'on'){
            try{
                $user = Sentinel::authenticateAndRemember($request->all());
            }catch(ThrottlingException $e){
                $delay = $e->getDelay();
                $returnData = array(
                     'status' => 'error',
                     'message' => 'Please review',
                     'errors' => ["You are banned for $delay seconds."]
                );
                //return response()->json($returnData, 500);
                return redirect()->back()->with(['error'=>"You are banned for $delay seconds."]);
            }catch(NotActivatedException $e){
                $returnData = array(
                    'status' => 'error',
                    'message' => 'Please review',
                    'errors' => ["Please activate your account."]
               );
               //return response()->json($returnData, 500);
               return redirect()->back()->with(['error'=>"Your account is not activated, Please activate your account."]);
           }
   
        } else {
            try{
                $user = Sentinel::authenticate($request->all());
            }catch(ThrottlingException $e){
                $delay = $e->getDelay();
                $returnData = array(
                     'status' => 'error',
                     'message' => 'Please review',
                     'errors' => ["You are banned for $delay seconds."]
                );
                //return response()->json($returnData, 500);
                return redirect()->back()->with(['error'=>"You are banned for $delay seconds."]);
            }catch(NotActivatedException $e){
                $returnData = array(
                    'status' => 'error',
                    'message' => 'Please review',
                    'errors' => ["Please activate your account."]
               );
               //return response()->json($returnData, 500);
               return redirect()->back()->with(['error'=>"Your account is not activated, Please activate your account."]);
           }
        }

        if(Sentinel::check()){
            return redirect(url('/'));
        } else{
            $returnData = array(
                'status' => 'error',
                'message' => 'Please review',
                'errors' => ["Email or Password is Mismatched."]
           );
           //return response()->json($returnData, 500);
           return redirect()->back()->with(['error'=>"Email or Password is Mismatched."]);
        }
    }

    public function logout(){
        Sentinel::logout();
        return redirect(url('/login'));
    }

    function dashboard(){
        $data = ['user'=>Admin::where('id','=', session('logout'))->first()];
        return view('admin.dashboard', $data);
    }

    function settings(){
        $data = ['user'=>Admin::where('id','=', session('logout'))->first()];
        return view('admin.settings', $data);
    }

    function profile(){
        $data = ['user'=>Admin::where('id','=', session('logout'))->first()];
        return view('admin.profile', $data);
    }
    function staff(){
        $data = ['user'=>Admin::where('id','=', session('logout'))->first()];
        return view('admin.staff', $data);
    }
}

