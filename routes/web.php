<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/logout', 'Security\LoginController@logout');

Route::group(['middleware' => ['visitors']], function(){

Route::get('/register', 'Security\RegisterController@register');
Route::post('/register', 'Security\RegisterController@registerUser');

Route::get('/login', 'Security\LoginController@login');
Route::post('/login', 'Security\LoginController@postLogin');


Route::get('/admin/dashboard','Security\LoginController@dashboard');
Route::get('/admin/settings','Security\LoginController@settings');
Route::get('/admin/profile','Security\LoginController@profile');
Route::get('/admin/staff','Security\LoginController@staff');

Route::get('/activate/{email}/{code}', 'Security\ActivationController@activate');
Route::get('/forgot_password', 'Security\ForgotPassword@forgot');
Route::post('/forgot_password', 'Security\ForgotPassword@password');
Route::get('/reset_password/{email}/{code}', 'Security\ForgotPassword@reset');
Route::post('/reset_password/{email}/{code}', 'Security\ForgotPassword@resetPassword');
});


Route::group(['middleware' => ['admin']], function(){

    Route::get('/report', 'Reports\ReportController@report');
});
